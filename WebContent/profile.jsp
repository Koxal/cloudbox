<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> CloudBox</title>
        <link rel="icon" href="img/g3.ico">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/ChooseAFile.css">
        <link rel="stylesheet" href="css/socialMedia.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
        <link rel="stylesheet" type="text/css" media="screen" href="css/home.css" />
</head>
<body>
<%
if(session.getAttribute("username")==null)
{
	response.sendRedirect("login.jsp");
}
%>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="welcome/index"><div><img src="img\g3.svg"><img src="img/logo-text.svg"></div></a>
			</div>
			<div class="collapse navbar-collapse" id="navbar1">
				<ul class="nav navbar-nav navbar-right social">
					<li><br>
						<form action="Logout">
							<input type="submit" name="submit" id="log-out" value="Logout">
						</form>
					</li>
				</ul>
			</div>
		</div>
		<br>
	</nav>
	<br/>
	<div class="container">
  		<div class="text-center">
  			<p ><h2 style="font-family: halfmoon_b, sans-serif; color: #006DF0;">Select file:</h2></p>
  			<center>
  				<div class="form-group">
  					<input type="file" name="file" id="file" class="input-file">
  					<label for="file" class="btn btn-tertiary js-labelFile">
    					<i class="icon fa fa-check"></i>
   						 <span class="js-fileName" style="font-family: halfmoon_b, sans-serif;">Choose a file</span>
  					</label>
				</div>
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                <script type="text/javascript" src="js/ChooseAFile.js"></script>
        	</center>
  		</div>
	</div>
	<div class="container">
  		<div class="row" style="text-align:center;color: white;font-weight:300;margin-bottom:30px;">
  			<center>
                <input type="submit" value="Upload" class="btn btn-large outline btn-danger radial-out" style="font-family: halfmoon_b, sans-serif;"/>
  			</center>
  		</div>
	</div>
	<!-- Uploaded file preview -->
	<div>
	    <embed id="UploadedFile" src="" width="390px" height="16px">
	</div>
	<div class="table-reponsive">
		<table class="table">
			<thead>
				<tr>
					<th><h3 style="font-family: halfmoon_b, sans-serif;">Filename</h3></th>
					<th><h3 style="font-family: halfmoon_b, sans-serif;">Type</h3></th>
					<th><h3 style="font-family: halfmoon_b, sans-serif;">Size <small>(MB)</small></h3></th>
					<th><h3 style="font-family: halfmoon_b, sans-serif;">Date Modified</h3></th>
					<th><h3 style="font-family: halfmoon_b, sans-serif;">Delete</h3></th>
				</tr>
			</thead>
		</table>  
  	</div>
	<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>
<footer id="foot" class="bottom-page">
		<h2><b>
			&copy;
			KOX Team
		</b></h2>
</footer>
</html>
