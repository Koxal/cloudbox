var _submit = document.getElementById('_submit'),
    _file = document.getElementById('_file'),
    _progress = document.getElementById('_progress');

var upload = function() {

    if (_file.files.length === 0) {
        return;
    }

    var data = new FormData();
    data.append('SelectedFile', _file.files[0]);

    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            try {
                var resp = JSON.parse(request.response);
            } catch (e) {
                var resp = {
                    status: 'error',
                    data: 'Unknown error occurred: [' + request.responseText + ']'
                };
            }
            console.log(resp.status + ': ' + resp.data);
        }
    };

    request.upload.addEventListener('progress', function(e) {
        _progress.style.width = Math.ceil(e.loaded / e.total) * 100 + '%';
    }, false);

    request.open('POST', './upload.php');
    request.send(data);
}

_submit.addEventListener('click', upload);

window.onload = resizeme

window.onresize = resizeme

function resizeme() {
    var a = document.getElementById("git-img");
    var b = document.getElementById("git-link");
    var c = document.getElementById("log-in");
    if (window.innerWidth > 850) {
        var forth = 0.15 * window.innerWidth;
        a.style.left = forth + "px";
        b.style.left = forth + 20 + "px";
        c.style.right = forth + "px";
    } else {
        var d = document.getElementById("logo-img");
        var e = document.getElementById("logo-txt");
        a.style.left = "0px";
        b.style.left = "20px";
        c.style.right = "0px";
        d.style.textAlign = "right";
        e.style.textAlign = "right";
    }
}

function loadXMLDoc(docPath) {
    var ajaxRequest;
    try {
        ajaxRequest = new XMLHttpRequest();

    } catch (e) {
        // Something went wrong
        alert("Your browser broke!");
        return false;
    }


    ajaxRequest.onreadystatechange = function() {
        if (ajaxRequest.readyState == 4) {
            var ajaxDisplay = document.getElementById("content");
            ajaxDisplay.innerHTML = ajaxRequest.responseText;
        }
    }

    ajaxRequest.open("GET", docPath, true);
    ajaxRequest.send();

    if (docPath == "login.php") {
        var a = document.getElementById("log-in");
        a.innerHTML = "Sign up";
        //a.setAttribute("id", "sign-up");


        a.onclick = function backToIndex() {
            window.location.href = "index.php";
            return false;
        }
    }
    return false;
}


// // a.onclick = function goToLogin() {
// //     //change log-in button to sign up
// //     a.innerHTML="Sign up";
// //     a.setAttribute("id","sign-up");
// //
// //     var mes = document.getElementById("message");
// //     var content = document.getElementById("contentSignup");
// //     var ifSigned = document.getElementById("ifSignup");
// //     var b = document.getElementById("sign-up");
// //     var form =document.getElementById("form");
// //
// //     content.innerHTML="Log in";
// //     content.setAttribute("id","contentLogin");
// //     mes.innerHTML="Welcome<br>Please Log in";
// //     ifSigned.parentNode.removeChild(ifSigned);
// //     form.style.height="250px";
// //
// //     b.onclick=function backToIndex() {
// //       window.location.href = "index.php";
// //     }
// //
// //     return false;
// //   }
// // }
// // }
// / }
// // }
// }
// // }
// / }
// // }
// }
// }
// // }
// / }
// // }
