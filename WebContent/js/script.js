window.onload = resizeme

window.onresize = resizeme

function resizeme() {
	var a = document.getElementById("git-img");
	var b = document.getElementById("log-in");
	if (window.innerWidth > 850) {
		var forth = 0.15 * window.innerWidth;
		a.style.left = forth + "px";
		b.style.right = forth + "px";
	} else {
		var c = document.getElementById("logo-img");
		var d = document.getElementById("logo-txt");
		a.style.left = "0px";
		b.style.right = "0px";
		c.style.textAlign = "right";
		d.style.textAlign = "right";
	}
}

function loadXMLDoc(docFlag) {

	var ajaxRequest;
	try {
		ajaxRequest = new XMLHttpRequest();

	} catch (e) {
		// Something went wrong
		alert("Your browser broke!");
		return false;
	}

	ajaxRequest.onreadystatechange = function() {
		if (ajaxRequest.readyState == 4) {
			var ajaxDisplay = document.getElementById("content");
			ajaxDisplay.innerHTML = ajaxRequest.responseText;
		}
	}

	ajaxRequest.open("GET", docFlag, true);
	ajaxRequest.send();
	
	var a = document.getElementById("log-in");
	

	if (a.innerHTML == "Log in") {
		a.innerHTML = "Sign up";
		a.onclick = function() {
			loadXMLDoc("signup.jsp");
		};

	} else {
		a.innerHTML = "Log in";
		a.onclick = function() {
			loadXMLDoc("login.jsp");
		};

	}
	return false;

}
