
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
	<title>CloudBox</title>
	<link rel="shortcut icon" href="img/g3.ico" />
	<script src="js/script.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="css/index.css" />
	<style>
		p.message0{color:red;};
		p.message1{color:green;}
	</style>
</head>
<body>
	<header class="shadow-box">
		<a href="https://bitbucket.org/Koxal/cloudbox" id="git-img"
			class="github-img"><img src="img/bitbucket.png"
			alt="github-logo" class="github-logo"></a> 
		<a href="index.jsp" id="logo-img"
			class="logo-img"><img src="img/g3.svg" alt="logo" class="logo"></a>
		<a href="index.jsp" id="logo-txt" class="logo-txt"><img
			src="img/logo-text.svg" alt="logo-text" class="logo-text"></a> 
		<a onclick='loadXMLDoc("login.jsp")' href="#" id="log-in"
			class="button-link">Log in</a>
	</header>
	<div id="content" class="content">
		<h1 id="message" class="contentHeader">Cloud platform<br> built from scratch</h1>
		<br>
		<form id="form" method="post" action='SignUp'>
			<h1 id="contentSignup" class="contentSignup">Sign up</h1>
			<input type="text" name="firstname" placeholder="Firstname" required="required" />
				<br>
			<input type="text" name="lastname" placeholder="Lastname" required="required" />
				<br>
			<input type="text" name="email" placeholder="Email-ID" required="required" />
				<br>
			<input type="password" name="password" pattern="[A-Za-z0-9]{6,25}" title="6 to 25 alphanumeric characters" placeholder="Password"  required="required" />
				<br>
			<button type="submit" class="Submit" name="submit">Submit</button>
			<p class="message1" style="font-family: 'halfmoon';color=#FF0000;">${message1}</p>	
			<p class="message0" style="font-family: 'halfmoon';color=#00FF00;">${message0}</p>		
		</form>
	</div>
	<footer id="foot" class="bottom-page">
		<h2>
			&copy;
			KOX Team
		</h2>
	</footer>
</body>
</html>