package servicii.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UserModel {
	private int user_id;
	private String user_type;
	private String user_fname;
	private String user_lname;
	private String user_email;
	
	
	public UserModel(int user_id, String user_type, String user_fname, String user_lname,
			String user_email)
	{
		this.user_id=user_id;
		this.user_type=user_type;
		this.user_fname=user_fname;
		this.user_lname=user_lname;
		this.user_email=user_email;
		
	}
	public int getUser_id()
	{
		return user_id;
	}
	public String getUser_type()
	{
		return user_type;
	}
	public String getUser_fname()
	{
		return user_fname;
	}
	public String getUser_lname()
	{
		return user_lname;
	}
	public String getUser_email()
	{
		return user_email;
	}
	
	//@Override
	public String toString()
	{
		return new com.google.gson.Gson().toJson(this);
	}
	
	
}

