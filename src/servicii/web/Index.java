package servicii.web;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// adnotarea Path specifica calea relativa spre un anumit serviciu web
// e.g., @ApplicationPath("rest") + @Path("/hello") => http://localhost:8080/ProiectServiciiWeb/rest/hello
// in aceasta situatie se va apela una din metodele clasei Hello, in functie de tipul media (text/plain, text/xml sau text/html)
@Path("/user")
public class Index {

	// Metoda apelata daca tipul media cerut este JSON
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUserList() {
		return DBManager.getInstance().getUserList().toString();
	}

	// Metoda apelata daca tipul media cerut este JSON
	@GET
	@Path("/file")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFileList() {
		return DBManager.getInstance().getFileList().toString();
	}
	
	// Metoda apelata daca tipul media cerut este JSON
	@GET
	@Path("/file/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFileByUserId(@PathParam("id") int id) {
		return DBManager.getInstance().getFilesByUserId(id).toString();
	}

	// Metoda apelata daca tipul media cerut este JSON
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public boolean deleteUserList() {
		if(deleteFileList())
			return DBManager.getInstance().deleteUserList();
		else
			return false;
	}
	
	// Metoda apelata daca tipul media cerut este JSON
	@DELETE
	@Path("/file")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean deleteFileList() {
		return DBManager.getInstance().deleteFileList();
	}
	
	@DELETE
	@Path("/file/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean deleteFileByUserId(@PathParam("id") int id) {
		return DBManager.getInstance().deleteFileByUserId(id);
	}

}
