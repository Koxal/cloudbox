package servicii.web;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FileModel {
	private String file_name;
	private String file_ext;
	private double file_size;
	private int user_id;
	private java.sql.Date file_time;
	
	public FileModel(String file_name, String file_ext,double file_size,Date file_time,int user_id)
	{
		this.file_name=file_name;
		this.file_ext=file_ext;
		this.file_size=file_size;
		this.file_time=file_time;
		this.user_id=user_id;
	}
	
	public String getFile_name()
	{
		return file_name;
	}
	
	public String getFile_ext()
	{
		return file_ext;
	}
	
	public double  getFile_size()
	{
		return file_size;
	}
	
	public Date getFile_time()
	{
		return file_time;
	}
	
	@Override
	public String toString(){
		return new com.google.gson.Gson().toJson(this);
	}
}
