package servicii.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.security.*;
import java.math.*;



public class DBManager {
	public static String messageError="Wrong password or e-mail address. Please try again!";
	private static final String URL = "jdbc:mysql://localhost:3306/cloudbox";
	private static final String USERNAME = "root";
	//private static final String PASSWORD = "Ilov3th3sun#";
	
	private static final DBManager instance = new DBManager();
	private Connection conn;
	public static DBManager getInstance() {
		return instance;
	}
	private DBManager() {
		System.out.println("Loading driver...");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException(
					"Cannot find the driver in the classpath!", e);
		}
		try {
			conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public List<UserModel> getUserList() {
		try (Statement st = conn.createStatement()) {
			List<UserModel> UserList = new ArrayList<UserModel>();
			st.execute("select * from users");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				UserModel User = new UserModel(rs.getInt("user_id"),
					rs.getString("user_type"), rs.getString("user_fname"),
					rs.getString("user_lname"), rs.getString("user_email"));
				UserList.add(User);
			}
			// st.close();
			return UserList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteUserList(){
		try(Statement st=conn.createStatement())
		{
			return st.execute("delete * from users");
			
		} catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	public List<FileModel> getFileList(){
		try(Statement st=conn.createStatement())
		{
			List<FileModel> FileList=new ArrayList<FileModel>();
			st.execute("select * from files");
			ResultSet rs=st.getResultSet();
			while(rs.next())
			{
				FileModel File=new FileModel(rs.getString("file_name"),
						rs.getString("file_ext"),rs.getDouble("file_size"),
						rs.getDate("file_time"),rs.getInt("user_id"));
				FileList.add(File);
			}
			return FileList;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteFileList(){
		try(Statement st=conn.createStatement())
		{
			return st.execute("delete * from files");
			
		} catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
	}
	
	public List<FileModel> getFilesByUserId(int id){
		try(Statement st=conn.createStatement())
		{
			List<FileModel> FileList = new ArrayList<FileModel>();
			st.execute("select * from files where user_id="+id);
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				FileModel File=new FileModel(rs.getString("file_name"),
						rs.getString("file_ext"),rs.getDouble("file_size"),
						rs.getDate("file_time"),rs.getInt("user_id"));
				FileList.add(File);
			}
			return FileList;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteFileByUserId(int id){
		try(Statement st=conn.createStatement())
		{
			return st.execute("delete from files where user_id="+id);
		} catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static String md5(String data){
		try{
			MessageDigest md=MessageDigest.getInstance("MD5");
			byte[] messageDigest=md.digest(data.getBytes());
			BigInteger num=new BigInteger(1,messageDigest);
			String hashtext=num.toString(16);
			while(hashtext.length()<32){
				hashtext="0"+hashtext;
			}
			return hashtext;
		}catch(NoSuchAlgorithmException e){
			throw new RuntimeException(e);
		}
	}
	@SuppressWarnings("finally")
	public List<String> testLogin(String user_email,String user_password){
		try(Statement st=conn.createStatement())
		{
			List<String> user_list=new ArrayList<String>();
			st.execute(("select * from users where user_email='")+user_email+("' && user_password='")+md5(user_password)+("';"));
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				user_list.add(rs.getString("user_id"));
				user_list.add(rs.getString("user_type"));
				user_list.add(rs.getString("user_fname"));
				user_list.add(rs.getString("user_lname"));
				user_list.add(rs.getString("user_email"));
				user_list.add(rs.getString("user_password"));
			}
			
			return user_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public int signUp(String user_fname,String user_lname, String user_email,String user_password){
		try(Statement st=conn.createStatement())
		{ 
			st.execute("insert into users(user_fname,user_lname,user_email,user_password) values(\""+user_fname+'"'+','+'"'+user_lname+'"'+','+'"'+user_email+'"'+','+'"'+md5(user_password)+'"'+");");
			return 1;
		}catch (SQLException e)
		{
			e.printStackTrace();
			System.out.println(e);
			return 0;
		}
	}
	
}
