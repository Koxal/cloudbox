package com.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import servicii.web.DBManager;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	public int result;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(result==1)
		{	
			String message="Succes register!";
	        request.setAttribute("message", message); // This will be available as ${message}
	        request.getRequestDispatcher("/WebContent/signup.jsp").forward(request, response);
	        System.out.println("Succes register!");
		}
		else
		{
			String message="Try again register!";
			request.setAttribute("message", message); // This will be available as ${message}
	        request.getRequestDispatcher("/WebContent/signup.jsp").forward(request, response);
	        System.out.println("Try again register!");
		}	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int reuslt;
		response.setContentType("text/html");
		String user_fname,user_lname,user_email,user_password;
		user_fname=request.getParameter("firstname").toString();
		user_lname=request.getParameter("lastname").toString();
		user_email=request.getParameter("email").toString();
		user_password=request.getParameter("password").toString();
		result=DBManager.getInstance().signUp(user_fname,user_lname,user_email,user_password);
		//response.sendRedirect("/CloudBox/index.jsp#");
		if(result==1)
		{
			String message1 = "You are Successfully Registered "+user_lname+"! Please login to access your Profile!";
	        request.setAttribute("message1", message1); // This will be available as ${message}
	        request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		else
		{
			String message0 = "Oops! Error. Please try again later!!!";
	        request.setAttribute("message0", message0); // This will be available as ${message}
	        request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
			
			
	}

}
