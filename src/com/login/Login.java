package com.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import servicii.web.DBManager;

@WebServlet("/Login")
public class Login extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username =request.getParameter("username");
		String password =request.getParameter("password");
		List<String> userList = DBManager.getInstance().testLogin(username, password);
		if(userList.get(1).equals("admin")&&userList.get(4).equals("koxteam@koxteam.com")&&userList.get(5).equals("5a358932576ba7be51cbe06a5b9222f0")){
			if(userList!=null){
				HttpSession session=request.getSession();
				session.setAttribute("username", username);
				response.sendRedirect("admin.jsp");
			}
			else
				response.sendRedirect("/CloudBox/index.jsp#");	
		}
		else
		{
			if(userList!=null){
				HttpSession session=request.getSession();
				session.setAttribute("username", username);
				response.sendRedirect("profile.jsp");
			}
			else
				response.sendRedirect("/CloudBox/index.jsp");
		}
	}
}
